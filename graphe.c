/*
  Structures de type graphe
  Structures de donnees de type liste
  (Pas de contrainte sur le nombre de noeuds des  graphes)
*/


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "graphe.h"


psommet_t chercher_sommet (pgraphe_t g, int label)
{
  psommet_t s ;

  s = g ;

  while ((s!= NULL) && (s->label != label))
    {
      s = s->sommet_suivant ;
    }
  return s ;
}

parc_t existence_arc (parc_t l, psommet_t s)
{
  parc_t p = l ;

  while (p != NULL)
    {
      if (p->dest == s)
	return p ;
      p = p->arc_suivant ;
    }
  return p ;
  
}


void ajouter_arc (psommet_t o, psommet_t d, int distance)
{
  parc_t parc ;

  parc = (parc_t) malloc (sizeof(arc_t)) ;

  if (existence_arc (o->liste_arcs, d) != NULL)
    {
      fprintf(stderr, "ajout d'un arc deja existant\n") ;
      exit (-1) ;
    }
  
  parc->poids = distance ;
  parc->dest = d ;
  parc->arc_suivant = o->liste_arcs ;
  o->liste_arcs = parc ;
  return ;
}



// ===================================================================

int nombre_sommets (pgraphe_t g)
{
  psommet_t p = g ;
  int nb = 0 ;

  while (p != NULL)
    {
      nb = nb + 1 ;
      p = p->sommet_suivant ;
    }

  return nb ;
}

int nombre_arcs (pgraphe_t g)
{

  psommet_t p = g ;
  int nb_arcs = 0 ;

  while (p != NULL)
    {
      parc_t l = p->liste_arcs ;

      while (l != NULL)
	{
          nb_arcs = nb_arcs + 1 ;
	  l = l->arc_suivant ;
	}
      
      p = p->sommet_suivant ;
    }
  return nb_arcs ;
}

void init_couleur_sommet (pgraphe_t g)
{
  psommet_t p = g ;

  while (p != NULL)
    {
      p->couleur = 0 ; // couleur indefinie
      p = p->sommet_suivant ; // passer au sommet suivant dans le graphe
    }
  
  return ;
}

int colorier_graphe (pgraphe_t g)
{
  /*
    coloriage du graphe g
    
    datasets
    graphe data/gr_planning
    graphe data/gr_sched1
    graphe data/gr_sched2
  */

  psommet_t p = g ;
  parc_t a ;
  int couleur ;
  int max_couleur = INT_MIN ; // -INFINI
  
  int change ;

  init_couleur_sommet (g) ;
  
  while (p != NULL)
    {
      couleur = 1 ; // 1 est la premiere couleur

      // Pour chaque sommet, on essaie de lui affecter la plus petite couleur

      // Choix de la couleur pour le sommet p
      
      do
	{
	  a = p->liste_arcs ;
	  change = 0 ;
      
	  while (a != NULL)
	    {
	      if (a->dest->couleur == couleur)
		{
		  couleur = couleur + 1 ;
		  change = 1 ;
		} 
	      a = a->arc_suivant ; 
	    }

	} while (change == 1) ;

      // couleur du sommet est differente des couleurs de tous les voisins
      
      p->couleur = couleur ;
      if (couleur > max_couleur)
	max_couleur = couleur ;

      p = p->sommet_suivant ;
    }
  
  return max_couleur ;
}

void afficher_graphe_largeur (pgraphe_t g, int r)
{
  /*
    afficher les sommets du graphe avec un parcours en largeur
  */
  //trouver le sommet avec label r
  if (g == NULL)
        return;

    // Initialisation des marqueurs de visite
    int *visite = (int*)malloc(nombre_sommets(g) * sizeof(int));
    for (int i = 0; i < nombre_sommets(g); ++i)
        visite[i] = 0;

    // File pour stocker les sommets à visiter
    int *file = (int*)malloc(nombre_sommets(g) * sizeof(int));
    int debut = 0, fin = 0;

    // Enfiler le sommet de départ
    file[fin++] = r;
    visite[r] = 1;

    while (debut < fin) {
        int sommet_courant = file[debut++];

        // Afficher le sommet visité
        printf("%d ", sommet_courant);

        // Parcourir les arcs sortants du sommet courant
        for (parc_t arc = g[sommet_courant].liste_arcs; arc != NULL; arc = arc->arc_suivant) {
            if (!visite[arc->dest->label]) {
                // Enfiler le sommet voisin non visité
                file[fin++] = arc->dest->label;
                visite[arc->dest->label] = 1;
            }
        }
    }

    free(visite);
    free(file);



  return;
}


void parcours_profondeur(psommet_t sommet, int visited[]) {
    // Marquer le sommet actuel comme visité
    visited[sommet->label] = 1;
    printf("%d ", sommet->label); // Afficher le label du sommet

    // Parcourir tous les sommets voisins du sommet actuel
    parc_t arc = sommet->liste_arcs;
    while (arc != NULL) {
        // Si le sommet voisin n'a pas encore été visité, effectuer un parcours en profondeur à partir de ce sommet
        if (visited[arc->dest->label] == 0) {
            parcours_profondeur(arc->dest, visited);
        }
        arc = arc->arc_suivant;
    }
}

void afficher_graphe_profondeur(pgraphe_t g, int r) {
    if (g == NULL) {
        printf("Le graphe est vide.\n");
        return;
    }

    // Initialiser un tableau pour marquer les sommets visités
    int *visited = (int*)calloc(nombre_sommets(g), sizeof(int));

    // Trouver le sommet de départ avec le label r
    psommet_t sommet_depart = chercher_sommet(g, r);

    if (sommet_depart == NULL) {
        printf("Le sommet de départ n'existe pas dans le graphe.\n");
        free(visited);
        return;
    }

    // Effectuer un parcours en profondeur à partir du sommet de départ
    printf("Parcours en profondeur à partir du sommet %d : ", r);
    parcours_profondeur(sommet_depart, visited);
    printf("\n");

    // Libérer la mémoire allouée pour le tableau visited
    free(visited);
}


void algo_dijkstra (pgraphe_t g, int r)
{
  /*
    algorithme de dijkstra
    des variables ou des chanmps doivent etre ajoutees dans les structures.
  */
  if (g == NULL)
        return;

    int nb_sommets = nombre_sommets(g);
    int *distance = (int*)malloc(nb_sommets * sizeof(int));
    int *visite = (int*)malloc(nb_sommets * sizeof(int));

    // Initialisation des distances à l'infini et des marqueurs de visite
    for (int i = 0; i < nb_sommets; ++i) {
        distance[i] = INT_MAX;
        visite[i] = 0;
    }

    distance[r] = 0;

    // Boucle principale
    for (int i = 0; i < nb_sommets - 1; ++i) {
        // Sélection du sommet non visité avec la plus petite distance
        int u = -1;
        for (int j = 0; j < nb_sommets; ++j) {
            if (!visite[j] && (u == -1 || distance[j] < distance[u]))
                u = j;
        }

        // Marquer le sommet sélectionné comme visité
        visite[u] = 1;

        // Mettre à jour les distances des sommets voisins non visités
        for (parc_t arc = g[u].liste_arcs; arc != NULL; arc = arc->arc_suivant) {
            int v = arc->dest->label;
            if (!visite[v] && distance[u] != INT_MAX && distance[u] + arc->poids < distance[v]) {
                distance[v] = distance[u] + arc->poids;
            }
        }
    }

    // Affichage des distances les plus courtes
    printf("Distances les plus courtes depuis le sommet %d :\n", r);
    for (int i = 0; i < nb_sommets; ++i) {
        if (distance[i] == INT_MAX)
            printf("Sommet %d : non accessible\n", i);
        else
            printf("Sommet %d : %d\n", i, distance[i]);
    }

    free(distance);
    free(visite);

  return ;
}





// ======================================================================




int degre_sortant_sommet(pgraphe_t g, psommet_t s) {
    int count = 0;
    // Parcourir la liste des arcs sortants du sommet s et compter leur nombre
    parc_t arc = s->liste_arcs;
    while (arc != NULL) {
        count++;
        arc = arc->arc_suivant;
    }
    return count;
}

int degre_entrant_sommet(pgraphe_t g, psommet_t s) {
    int count = 0;
    // Parcourir tous les sommets du graphe g
    psommet_t current = g;
    while (current != NULL) {
        // Parcourir la liste des arcs sortants de chaque sommet et vérifier si l'un d'entre eux mène au sommet s
        parc_t arc = current->liste_arcs;
        while (arc != NULL) {
            if (arc->dest == s) {
                count++;
                break; // Arrêter la boucle dès qu'un arc menant à s est trouvé
            }
            arc = arc->arc_suivant;
        }
        current = current->sommet_suivant;
    }
    return count;
}

int degre_maximal_graphe(pgraphe_t g) {
    int max_degre = 0;
    // Parcourir tous les sommets du graphe g
    psommet_t current = g;
    while (current != NULL) {
        // Calculer le degré sortant du sommet courant
        int degre = degre_sortant_sommet(g, current);
        // Mettre à jour le degré maximal si nécessaire
        if (degre > max_degre) {
            max_degre = degre;
        }
        current = current->sommet_suivant;
    }
    return max_degre;
}

int degre_minimal_graphe(pgraphe_t g) {
    int min_degre = INT_MAX; // Initialiser avec une valeur maximale
    // Parcourir tous les sommets du graphe g
    psommet_t current = g;
    while (current != NULL) {
        // Calculer le degré sortant du sommet courant
        int degre = degre_sortant_sommet(g, current);
        // Mettre à jour le degré minimal si nécessaire
        if (degre < min_degre) {
            min_degre = degre;
        }
        current = current->sommet_suivant;
    }
    return min_degre;
}

int independant(pgraphe_t g) {
    // Pour chaque paire de sommets (i, j), vérifier s'il existe un arc entre eux
    psommet_t current = g;
    while (current != NULL) {
        parc_t arc = current->liste_arcs;
        while (arc != NULL) {
            // Vérifier si l'arc mène à un autre sommet du graphe
            if (chercher_sommet(g, arc->dest->label) != NULL) {
                return 0; // S'il existe un arc reliant deux sommets, le graphe n'est pas indépendant
            }
            arc = arc->arc_suivant;
        }
        current = current->sommet_suivant;
    }
    return 1; // Si aucun arc ne relie deux sommets, le graphe est indépendant
}


int complet(pgraphe_t g) {
    // Parcourir tous les sommets du graphe
    psommet_t current = g;
    while (current != NULL) {
        // Pour chaque sommet, vérifier s'il est relié à tous les autres sommets
        psommet_t temp = g;
        while (temp != NULL) {
            // Si le sommet courant est différent du sommet temporaire et s'il n'y a pas d'arc entre eux, le graphe n'est pas complet
            if (current != temp && existence_arc(current->liste_arcs, temp) == NULL) {
                return 0;
            }
            temp = temp->sommet_suivant;
        }
        current = current->sommet_suivant;
    }
    return 1; // Si tous les sommets sont reliés entre eux, le graphe est complet
}


int regulier(pgraphe_t g) {
    // Calculer le degré sortant du premier sommet du graphe
    int degre_reference = degre_sortant_sommet(g, g);
    // Parcourir tous les sommets du graphe et vérifier si leur degré sortant est différent du degré de référence
    psommet_t current = g->sommet_suivant;
    while (current != NULL) {
        // Si le degré sortant du sommet courant est différent du degré de référence, le graphe n'est pas régulier
        if (degre_sortant_sommet(g, current) != degre_reference) {
            return 0;
        }
        current = current->sommet_suivant;
    }
    return 1; // Si tous les sommets ont le même degré sortant, le graphe est régulier
}




/*
  placer les fonctions de l'examen 2017 juste apres
*/

int elementaire(pgraphe_t g, chemin_t c) {
    int *sommets = c.sommets;
    int taille = c.taille;

    // Tableau pour stocker les sommets déjà visités
    int visite[nombre_sommets(g)];
    for (int i = 0; i < nombre_sommets(g); i++) {
        visite[i] = 0; // Initialisation à 0 (non visité)
    }

    // Vérifier si tous les sommets sont distincts
    for (int i = 0; i < taille; i++) {
        if (visite[sommets[i]] == 1) {
            return 0; // Le chemin n'est pas élémentaire
        } else {
            visite[sommets[i]] = 1; // Marquer le sommet comme visité
        }
    }

    return 1; // Le chemin est élémentaire
}
int simple(pgraphe_t g, chemin_t c) {
    int *sommets = c.sommets;
    int taille = c.taille;

    // Vérifier si tous les arcs du chemin sont distincts
    for (int i = 0; i < taille - 1; i++) {
        psommet_t sommet_actuel = chercher_sommet(g, sommets[i]);
        psommet_t sommet_suivant = chercher_sommet(g, sommets[i + 1]);

        // Vérifier si l'arc entre le sommet actuel et le sommet suivant existe
        parc_t arc = existence_arc(sommet_actuel->liste_arcs, sommet_suivant);
        if (arc == NULL) {
            return 0; // L'arc entre ces sommets n'existe pas, le chemin n'est pas simple
        }
    }

    return 1; // Le chemin est simple
}
int eulerien(pgraphe_t g, chemin_t c) {
    int *sommets = c.sommets;
    int taille = c.taille;

    // Vérifier si chaque sommet est degré pair (sauf le premier et le dernier sommet)
    for (int i = 1; i < taille - 1; i++) {
        psommet_t sommet = chercher_sommet(g, sommets[i]);
        int degre_sortant = degre_sortant_sommet(g, sommet);
        int degre_entrant = degre_entrant_sommet(g, sommet);

        // Si le degré du sommet n'est pas pair, le chemin n'est pas eulérien
        if ((degre_sortant + degre_entrant) % 2 != 0) {
            return 0;
        }
    }

    // Vérifier si le premier et le dernier sommet ont un degré impair
    psommet_t premier_sommet = chercher_sommet(g, sommets[0]);
    psommet_t dernier_sommet = chercher_sommet(g, sommets[taille - 1]);
    int degre_sortant_premier = degre_sortant_sommet(g, premier_sommet);
    int degre_sortant_dernier = degre_sortant_sommet(g, dernier_sommet);
    int degre_entrant_premier = degre_entrant_sommet(g, premier_sommet);
    int degre_entrant_dernier = degre_entrant_sommet(g, dernier_sommet);

    // Si l'un des deux sommets a un degré impair, le chemin n'est pas eulérien
    if ((degre_sortant_premier + degre_entrant_premier) % 2 != 0 || 
        (degre_sortant_dernier + degre_entrant_dernier) % 2 != 0) {
        return 0;
    }

    // Si toutes les conditions sont remplies, le chemin est eulérien
    return 1;
}
int hamiltonien(pgraphe_t g, chemin_t c) {
    int *sommets = c.sommets;
    int taille = c.taille;
    int sommets_visites[nombre_sommets(g)];
    int nb_sommets_visites = 0;

    // Initialiser le tableau des sommets visités
    for (int i = 0; i < nombre_sommets(g); i++) {
        sommets_visites[i] = 0;
    }

    // Vérifier si tous les sommets du chemin sont distincts
    for (int i = 0; i < taille; i++) {
        if (sommets_visites[sommets[i]] == 1) {
            return 0; // Le chemin n'est pas hamiltonien car un sommet est visité plus d'une fois
        } else {
            sommets_visites[sommets[i]] = 1; // Marquer le sommet comme visité
            nb_sommets_visites++;
        }
    }

    // Vérifier si tous les sommets du graphe ont été visités une fois
    if (nb_sommets_visites != nombre_sommets(g)) {
        return 0; // Le chemin n'est pas hamiltonien car tous les sommets ne sont pas visités
    }

    // Vérifier si le chemin est cyclique (le premier et le dernier sommet sont identiques)
    if (sommets[0] != sommets[taille - 1]) {
        return 0; // Le chemin n'est pas hamiltonien car il n'est pas cyclique
    }

    return 1; // Le chemin est hamiltonien
}
int graphe_eulerien(pgraphe_t g) {
    // Vérifier si chaque sommet du graphe a un degré pair
    psommet_t sommet = g;
    while (sommet != NULL) {
        // Vérifier si le degré du sommet est pair
        if ((degre_sortant_sommet(g, sommet) + degre_entrant_sommet(g, sommet)) % 2 != 0) {
            return 0; // Le graphe n'est pas eulérien car au moins un sommet a un degré impair
        }
        sommet = sommet->sommet_suivant; // Passer au sommet suivant dans le graphe
    }

    return 1; // Le graphe est eulérien
}
int graphe_hamiltonien(pgraphe_t g) {
    int nb_sommets = nombre_sommets(g);

    // Vérifier si le graphe a au moins 3 sommets
    if (nb_sommets < 3) {
        return 0; // Le graphe n'est pas hamiltonien s'il a moins de 3 sommets
    }

    // Vérifier si le graphe est complet
    if (!complet(g)) {
        return 0; // Le graphe n'est pas hamiltonien s'il n'est pas complet
    }

    // Vérifier si chaque sommet du graphe a un degré au moins égal à (n-1)
    psommet_t sommet = g;
    while (sommet != NULL) {
        // Vérifier si le degré du sommet est au moins égal à (n-1)
        if ((degre_sortant_sommet(g, sommet) + degre_entrant_sommet(g, sommet)) < nb_sommets - 1) {
            return 0; // Le graphe n'est pas hamiltonien si au moins un sommet a un degré inférieur à (n-1)
        }
        sommet = sommet->sommet_suivant; // Passer au sommet suivant dans le graphe
    }

    return 1; // Le graphe est hamiltonien
}
int distance(pgraphe_t g, int x, int y) {
    psommet_t sommet_x = chercher_sommet(g, x);
    psommet_t sommet_y = chercher_sommet(g, y);

    // Vérifier si les sommets avec les labels x et y existent dans le graphe
    if (sommet_x == NULL || sommet_y == NULL) {
        return -1; // Les sommets avec les labels x et y n'existent pas dans le graphe
    }

    // Effectuer un parcours en largeur ou en profondeur pour trouver le plus court chemin entre x et y
    // Utiliser l'algorithme de Dijkstra pour obtenir la distance la plus courte entre x et y

    return 0; // Placeholder, remplacer par le calcul réel de la distance
}
int excentricite(pgraphe_t g, int n) {
    psommet_t sommet = chercher_sommet(g, n);

    // Vérifier si le sommet avec le label n existe dans le graphe
    if (sommet == NULL) {
        return -1; // Le sommet avec le label n n'existe pas dans le graphe
    }

    // Initialiser la distance maximale à 0
    int max_distance = 0;

    // Parcourir tous les sommets du graphe et calculer la distance maximale du sommet donné
    psommet_t current = g;
    while (current != NULL) {
        // Ignorer le sommet lui-même
        if (current->label != n) {
            // Calculer la distance entre le sommet donné et le sommet courant
            int distan = distance(g, n, current->label);
            // Mettre à jour la distance maximale si la distance calculée est plus grande
            if (distan > max_distance) {
                max_distance = distan;
            }
        }
        current = current->sommet_suivant; // Passer au sommet suivant dans le graphe
    }

    return max_distance; // Renvoyer l'excentricité du sommet de label n dans le graphe g
}
int diametre(pgraphe_t g) {
    int max_excentricite = 0;

    // Parcourir tous les sommets du graphe et calculer l'excentricité maximale
    psommet_t current = g;
    while (current != NULL) {
        // Calculer l'excentricité pour le sommet courant
        int excentricite_sommet = excentricite(g, current->label);
        // Mettre à jour l'excentricité maximale si l'excentricité du sommet courant est plus grande
        if (excentricite_sommet > max_excentricite) {
            max_excentricite = excentricite_sommet;
        }
        current = current->sommet_suivant; // Passer au sommet suivant dans le graphe
    }

    return max_excentricite; // Renvoyer le diamètre du graphe
}
